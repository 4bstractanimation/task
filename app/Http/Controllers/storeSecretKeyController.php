<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
Use App\secrets;


class storeSecretKeyController extends Controller
{
    public function storeSecretKey(request $request){

    $private_key = Storage::get("keys/private_key.pem");
    $input = new secrets( $request->all() );

    $username= $input["username"];
    $secretName= $input["secretName"];
    $encryptedSecret= $input["encryptedSecret"];


    $encryptedSecret = base64_decode($encryptedSecret);

    if(openssl_private_decrypt( $encryptedSecret, $decryptedSecret, $private_key )){
            if($username && $secretName && $encryptedSecret){
                $input->save();
                return response()->json(['message'=>"message saved"], 200);
            }else{
                return response()->json(['message'=>"All fields are required"], 200);
            }
    }else{
            return response()->json(['message'=>"Use Correct Key to Encrypt"], 200);
         }
    }
}
