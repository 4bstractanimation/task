<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class getServerKeyController extends Controller
{
    public function getServerKey() {

    $public_key = Storage::get("keys/pub_key.pem");

    return response()->json(['public_key'=>$public_key], 200);
}
}
