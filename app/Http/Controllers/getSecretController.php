<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
Use App\secrets;

class getSecretController extends Controller
{
    public function getSecret(request $request){

    $private_key =  Storage::get("keys/private_key.pem");

    $input = $request->all();

    $username= $input["username"];
    $secretName= $input["secretName"];

    $encryptedSecrets = secrets::where("username", $username)->where("secretName", $secretName)->get();


    $encryptedSecret = "";
    foreach($encryptedSecrets as $sec){
        $encryptedSecret = $sec->encryptedSecret;
    }

    $encryptedSecret = base64_decode($encryptedSecret);

    $decryptedSecret = "";

    openssl_private_decrypt(
            $encryptedSecret, // message to decrypt
            $decryptedSecret, // &decrypted message
            $private_key  // private key
    );

    if(!$username && !$secretName ){
        return response()->json(['message'=>"All fields are required"], 200);
    }
    if(!$decryptedSecret){
        return response()->json(['message'=>"User With Asked Secret does not exist"], 200);
    }
    else{
        return response()->json(['secret'=>$decryptedSecret ], 200);
    }
    }
}
