<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Validator;
use Illuminate\Support\Facades\Storage;
use \Illuminate\Database\QueryException;
use App\Library\Services\Register;


class RegisterController extends Controller
{

function Register(request $request){
        $public_key = Storage::get("keys/pub_key.pem");

        $validator = Validator::make($request->all(), [ 'name' => 'required', 'email' => 'required|email', 'password' => 'required', 'confirm_password' => 'required|same:password', ]);
        if ($validator->fails()) { return response()->json(['error'=>$validator->errors()], 401); }
        $input = $request->all();

        if( array_key_exists('public_key', $input) ){
            if( $input['public_key'] != trim($public_key,"\n") ){
                    return response()->json(['error'=>"Wrong Public Key"], 401);
            }
        }else{
            return response()->json(['error'=>"Required Public Key"], 402);
        }

        $input['password'] = bcrypt($input['password']);
        try{
        $user = User::create($input);
        }catch(QueryException $e){
            return response()->json(['error'=>$e->getMessage()], 200);
        }

        $success['token'] = $user->createToken('myApp')-> accessToken;
        $success['name'] = $user->name;
        return response()->json(['success'=>$success], 200);
}
    }


