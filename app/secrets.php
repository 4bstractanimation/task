<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class secrets extends Model
{
     protected $fillable = ['username', 'secretName', 'encryptedSecret'];
}
