<?php

namespace App\Console\Commands;
use Illuminate\Support\Facades\Storage;
use Illuminate\Console\Command;

class myapp extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'laravel:start';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */

     public function handle(){
        $option = $this->choice('Choose the following', ['Register', 'getServerKey', 'storeSecret', 'getSecret']);
        if ($option == "Register") {
            $name = $this->ask('Enter Name');
            $email = $this->ask("Enter Email");
            $password = $this->secret('Enter password?');
            $confirm_password = $this-> secret("Re-enter password");
            $key = $this-> ask("Enter key");
            $url = 'http://127.0.0.1:8000/api/register';
            $data = array('name' => $name, 'email' => $email, 'password' => $password, 'confirm_password' => $confirm_password , 'public_key' => /* Storage::get("keys/pub_key.pem") */ $key );
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
            $result = curl_exec($ch);
            curl_close($ch);
            echo($result);
        }
        elseif($option == "getServerKey"){
            echo  file_get_contents("http://127.0.0.1:8000/api/getServerKey");
        }
        elseif($option == "storeSecret"){

            $username = $this->ask('Enter username');
            $secretName = $this->ask("Enter secretName");
            $encryptedSecret = $this->ask('Enter encryptedSecret?');
            $url = 'http://127.0.0.1:8000/api/storeSecret';
            $data = array('username' => $username, 'secretName' => $secretName, 'encryptedSecret' => $encryptedSecret );
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
            $result = curl_exec($ch);
            curl_close($ch);
            echo($result);
        }
        elseif($option == "getSecret"){
            $username = $this->ask('Enter username');
            $secretName = $this->ask("Enter secretName");
            $url = 'http://127.0.0.1:8000/api/getSecret';
            $data = array('username' => $username, 'secretName' => $secretName );
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
            $result = curl_exec($ch);
            curl_close($ch);
            echo($result);
        }
    }
}
