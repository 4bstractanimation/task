<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('register', 'registerController@Register');

Route::get('getServerKey', 'getServerKeyController@getServerKey');

Route::post('storeSecret' , 'storeSecretKeyController@storeSecretKey');

Route::post('getSecret' , 'getSecretController@getSecret');

Route::get('ensec' , function(){

    $encryptedSecret = "";

    $public_key= Storage::get("keys/pub_key.pem");
    openssl_public_encrypt(
            "New Secret", // message to encrypt
            $encryptedSecret, // &decrypted message
            $public_key  // private key
    );
    return response()->json(['message'=> base64_encode($encryptedSecret) ], 200);
});


Route::get("key", function(){
    $res = openssl_pkey_new(array('private_key_bits' => 2048));
    openssl_pkey_export($res, $privateKey);
    $publicKey = openssl_pkey_get_details($res);
    Storage::disk('local')->put('keys/pub_key.pem', $publicKey["key"]);
    Storage::disk('local')->put('keys/private_key.pem', $privateKey);
    return response()->json(['message'=>"All fields are required"], 200);
});
